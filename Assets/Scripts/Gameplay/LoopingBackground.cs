﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[AddComponentMenu("Playground/Gameplay/Looping Background")]
[RequireComponent(typeof(BoxCollider2D))]
public class LoopingBackground : MonoBehaviour
{ private Camera _camera;
    private Vector2 _screenSize;
    private Vector2 _bgBounds;
    private Vector2 _thresholds;
    private bool _reposition = false;

    void Start(){
        BoxCollider2D _boxCollider2D = GetComponent<BoxCollider2D>();
        _boxCollider2D.isTrigger = true;
        _bgBounds = new Vector2(_boxCollider2D.size.x*transform.localScale.x,_boxCollider2D.size.y*transform.localScale.y);
        //Calculate camera's screen size
        _camera = Camera.main;
        Vector3 bottomLeft = _camera.ScreenToWorldPoint(Vector3.zero);
        Vector3 topRight = _camera.ScreenToWorldPoint(new Vector3(_camera.pixelWidth, _camera.pixelHeight));
        _screenSize = new Vector2(topRight.x - bottomLeft.x, topRight.y - bottomLeft.y);
        _thresholds = new Vector2(_bgBounds.x/2+_screenSize.x/2,_bgBounds.y/2+_screenSize.y/2);
    }

    private void FixedUpdate()
    {
        if(!IsVisible()&& IsAtLeftOfCamera())
            _reposition = true;
    }

    private void LateUpdate()
    {
        if(_reposition)
        {
            Vector2 pos = transform.position;
            pos.x+=_bgBounds.x*2;
            transform.position = pos;
            _reposition = false;
        }
    }

    bool IsVisible(){
        return (Vector2.Distance(transform.position,_camera.transform.position)<_thresholds.x);
    }

    bool IsAtLeftOfCamera(){
        return (transform.position.x<_camera.transform.position.x);
    }
    
}
